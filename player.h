#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <stdio.h>
#include "common.h"
#include "board.h"
#include <vector>
using namespace std;

class Player {

private:
	Board board;
	Side side;
	Side o_side;


public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    void miniMax(Board b, Side s, int depth, vector<int> *sub);
    std::vector<Move*> trim(Board b, vector<Move *> m, int k);
    std::vector<Move*> noCorners(Board b, vector<Move *> m, int y);
    std::vector<Move*> getLegalMoves(Board b, Side s);
    int getScoreAfterMove(Board *b, Move *m, Side s);
    void setBoard(Board *b);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
