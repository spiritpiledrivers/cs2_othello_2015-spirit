Together Suraj and Peter worked to create an AI that would return a single move in order to have a base for the AI working.

Then Peter with some input from Suraj worked to implement a 2-dimensional minimax as described in the first part of the assignment. He sorted the code out into different helper functions to try to make it tidy.

Suraj then worked to implement a recursive method that would run the same minimax step numerous times. This would allow our AI to look farther into the future of the game and make a decision without writing a significant amount of code. 

Together we worked to add some heuristics that we believe would give it the upperhand much later in the game. These are usually moves that are made early in the game that may not be recognized as valuable until the end of the game.

One of these heuristic improvements that we included was putting a heavy weight on the corner cells. We think that this gives a great advantage as more rocks are flipped over after each successive move at later points in the game. We also placed a higher value on the edge pieces. These are valuable since they can clear out an entire row to a single color.