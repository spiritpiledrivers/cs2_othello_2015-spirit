
#include "player.h"
#include "stdlib.h"
#include <vector>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    
	board = Board();
	this->side = side;
	if (side == BLACK)
		o_side = WHITE;
	else
		o_side = BLACK;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
     
    // Update board for the opponent's move if not NULL
    if (opponentsMove != NULL)
    {
		board.doMove(opponentsMove, o_side);
	}
	
	// Find legal moves that our AI can possibly take
	// For each of these moves check to see all possible moves by the opponent
	// Select our sub-branch such that the opponents score is minimized
	std::vector<Move*> moves = getLegalMoves(board, side);
	std::vector<int> sub_score = std::vector<int>();
	
	// In the case that there are no moves available for us to do
	// We return null
	if (moves.size() == 0)
		return NULL;
	
	
	// Add a level of greed when the opponent has given us the chance
	// to grab a corner piece
	for (unsigned int i = 0; i < moves.size(); i++)
	{
	if ((moves[i]->getX() == 0 && moves[i]->getY() == 0) || (moves[i]->getX() == 0 && moves[i]->getY() == 7) || 
		(moves[i]->getX() == 7 && moves[i]->getY() == 0) || (moves[i]->getX() == 7 && moves[i]->getY() == 7))
	{
		board.doMove(moves[i], side);
		return moves[i];
	}
	}
	moves=noCorners(board, moves, 2);
	while(moves.size()>3)
	{
		moves=trim(board, moves, 3);fprintf(stderr, "P\n");
	}
	
	
	// Initialize the sub_scores at an arbitrarily large (unreachable) score
	for (unsigned int i = 0; i < moves.size(); i++)
	{
		sub_score.push_back(200);
	}
	
	for (unsigned int i = 0; i < moves.size(); i++)
	{
		Board *temp = board.copy();
		temp->doMove(moves[i], side);
		std::vector<int> *sub = new std::vector<int>();
		miniMax(*temp, o_side, 5, sub);
		int min=100;
		for(unsigned int j=0; j<sub->size();j++)
		{
			if(sub->at(j)<min){min=sub->at(j);}
		}
		delete sub;
		sub_score[i]=min;
		delete temp;
	}
	
	// Now we pick our best move with the largest minimum per sub tree. We're
	// minimizing the opponent's best move possible
	int max_index = 0;
	
	for (unsigned int i = 0; i < sub_score.size(); i++)
	{
		fprintf(stderr, "(%d, %d): %d\n", moves[i]->getX(), moves[i]->getY(), sub_score[i]);
		if (sub_score[i] > sub_score[max_index])
			max_index = i;
	}
	
	board.doMove(moves[max_index], side);
	return moves[max_index];
}
std::vector<Move*> Player::noCorners(Board b, vector<Move *> moves, int y)
{
	std::vector<Move *> m = std::vector<Move *>();fprintf(stderr, "A\n");
	for (unsigned int i = 0; i < moves.size(); i++)
	{
		Board *temp = b.copy();
		temp->doMove(moves[i], side);
		std::vector<Move *> o = getLegalMoves(*temp, o_side);
		for(unsigned int j=0; j<o.size();j++)
		{
			if(!((o[j]->getX() == 0 && o[j]->getY() == 0) || (o[j]->getX() == 0 && o[j]->getY() == 7)
		|| (o[j]->getX() == 7 && o[j]->getY() == 0) || (o[j]->getX() == 7 && o[j]->getY() == 7)))
			{m.push_back(moves[i]); j=o.size();}
		}
		delete temp;
	}
	return m;
}
std::vector<Move*> Player::trim(Board b, vector<Move *> moves, int k)
{
	std::vector<int> sub_score = std::vector<int>();
	std::vector<Move *> m = std::vector<Move *>();fprintf(stderr, "A\n");
	for (unsigned int i = 0; i < moves.size(); i++)
	{
		Board *temp = b.copy();
		temp->doMove(moves[i], side);
		std::vector<int> *sub = new std::vector<int>();fprintf(stderr, "K\n");
		miniMax(*temp, o_side, k, sub);
		int min=100;fprintf(stderr, "B\n");
		for(unsigned int j=0; j<sub->size();j++)
		{
			if(sub->at(j)<min){min=sub->at(j);}
		}fprintf(stderr, "C\n");
		delete sub;
		sub_score.push_back(min);
		delete temp;
	}
	fprintf(stderr, "E\n");
	for (unsigned int i = 0; i < (moves.size()/2); i++)
	{
		int max=-200;
		int x;
		for (unsigned int j = 0; j < moves.size(); j++)
		{
			if(sub_score[j]>max){max=sub_score[j];x=j;}
		}
		m.push_back(moves[x]);
	}
	fprintf(stderr, "F\n");
	return m;
	
}

/**
 * @brief Minimax
 */
void Player::miniMax(Board b, Side s, int depth, vector<int> *sub)
{
	std::vector<Move*> moves = getLegalMoves(b, s);
	for(unsigned int i=0; i<moves.size(); i+=1)
	{
		if(moves[i]!=NULL)
		{
		Board *temp = b.copy();
		temp->doMove(moves[i], s);
		if(temp->isDone() || depth==1)
		{
			int o=getScoreAfterMove(&b, moves[i], s);
			sub->push_back(o);
		}
		else if(s==o_side)
		{
			int o = getScoreAfterMove(&b, moves[i], s);
			// This move gives the opponent too much of an advantage, 
			// stop pursuing this path if the score is already below
			// a threshhold
			if (o < -30) {sub->push_back(o);}
			else {miniMax(*temp, side, depth-1, sub);}
		}
		else
		{
			miniMax(*temp, o_side, depth-1, sub);
		}
		delete temp;
		}
	}
	for(unsigned i=0;i<moves.size();i++)
	{delete moves[i];}
}

/**
 * @brief Finds all the legal moves for a player given a board
 */
std::vector<Move*> Player::getLegalMoves(Board b, Side s)
{
	std::vector<Move*> moves = std::vector<Move*>();
	
	for (int i = 0; i < 8; i++) 
	{		
		for (int j = 0; j < 8; j++) 
		{
			Move *m = new Move(i, j);
			if (b.checkMove(m, s))
			{
				moves.push_back(m);
			}
		}
	}
	
	return moves;
}

/**
 * @brief Calculates the score on the board if a move is played by either
 * side. Does not alter the board.
 * 
 * @param b - The board of interest. A copy will be created and deleted.
 * @param m - The move that will be played on a copy of the board.
 * @param s - The side for which the move will be made.
 */
int Player::getScoreAfterMove(Board *b, Move *m, Side s)
{
	// Make a copy of the burrent board
	Board *t = b->copy();
	// Update the board with our new move assuming that it's legal
	t->doMove(m, s);
	
	int sum = t->count(side) - t->count(o_side);
	delete t;

	if ((m->getX() == 0) || (m->getY() == 0) || (m->getX() == 7) || (m->getY() == 7))
	{
		if(s==o_side){sum-=5;}
		else {sum+=5;}
	}
	else if ((m->getX() == 0 && m->getY() == 0) || (m->getX() == 0 && m->getY() == 7)
		|| (m->getX() == 7 && m->getY() == 0) || (m->getX() == 7 && m->getY() == 7))
	{
		if(s==o_side){sum-=40;}
		else {sum+=40;}
	}
	
	return sum;
}

/**
 * @brief Sets the board in play to a given board. This is mostly to work
 * with the testminimax file
 */
void Player::setBoard(Board *b)
{
	board = *b;
}
